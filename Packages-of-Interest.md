# Sublime​Code​Intel
- https://packagecontrol.io/packages/SublimeCodeIntel

# Sublime​Linter
- https://packagecontrol.io/packages/SublimeLinter

# Git​Savvy
- https://packagecontrol.io/packages/GitSavvy

# Plain​Tasks
- https://packagecontrol.io/packages/PlainTasks